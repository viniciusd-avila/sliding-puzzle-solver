# Sliding Puzzle Solver

Common Lisp implementation of the [A\* search algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm) to solve n-by-n sliding puzzles.
